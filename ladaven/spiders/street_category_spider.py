import scrapy
import json,httplib

class QuotesSpider(scrapy.Spider):
    
    name = "street_category"
    application_id = "iVF27T7ctPbAy1FleUCi8BUhg0phe0DWndZUcPv4"
    rest_api_key = "hdh12Jvkpx2gDJwdlOKeWyIahtFcNst7gJs38Acf"
    base = 'http://www.11street.my/'

    def start_requests(self):
        yield scrapy.Request(url=self.base, callback=self.parse_page)
                

    def parse_page(self, response):
        categories = response.xpath('//div[@class="category"]/ul[@class="cate"]/li')
        for category in categories:
            #import pdb; pdb.set_trace()
            url = category.xpath("a/@href").extract()[0]
            if url:
                yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        subcategories = response.xpath('//nav[@class="lnb"]/menu/li/ul/li/a')
        for sub in subcategories:
            #import pdb; pdb.set_trace()
            url = sub.xpath("@href").extract()[0]
            data = {
                'name': url.split("/")[2],
                'url': "%s" % url,
                'web': '11street',
            }
            connection = httplib.HTTPSConnection('api.parse.com', 443)
            connection.connect()
            connection.request('POST', '/1/classes/categories', json.dumps(data), {
                   "X-Parse-Application-Id": self.application_id,
                   "X-Parse-REST-API-Key": self.rest_api_key,
                   "Content-Type": "application/json"
                 })
            results = json.loads(connection.getresponse().read())
            print results
        
