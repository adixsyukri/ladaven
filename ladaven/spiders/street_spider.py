import scrapy
import json,httplib,urllib

class QuotesSpider(scrapy.Spider):
    
    name = "street"
    application_id = "iVF27T7ctPbAy1FleUCi8BUhg0phe0DWndZUcPv4"
    rest_api_key = "hdh12Jvkpx2gDJwdlOKeWyIahtFcNst7gJs38Acf"
    base = 'http://www.11street.my'

    def start_requests(self):
        connection = httplib.HTTPSConnection('api.parse.com', 443)
        params = urllib.urlencode({"where":json.dumps({
           "web": "11street"
         })})
        connection.connect()
        connection.request('GET', '/1/classes/categories?%s' % params,'',{
                        "X-Parse-Application-Id": self.application_id,
                        "X-Parse-REST-API-Key": self.rest_api_key,
                        "Content-Type": "application/json"
                        })
        data = json.loads(connection.getresponse().read())
        
        results = data['results']
        for result in results:
            if result['web'] == '11street':
                yield scrapy.Request(url=self.base+result['url'], callback=self.parse)

    def parse(self, response):
        products = response.xpath('//div[@id="product_listing"]/ul/li/div/div/div[@class="info"]')
        category = response.url.split("/")[4]
        if products:
            for product in products:
                description = product.xpath('a/div[@class="product-card__description"]')
                data = {
                    'name': product.xpath("h3/a/text()").extract()[0],
                    'price': product.xpath("div/strong/text()").extract()[0],
                    'category': category,
                    'url': product.xpath("h3/a/@href").extract()[0],
                    'web': '11street',
                }
                connection = httplib.HTTPSConnection('api.parse.com', 443)
                connection.connect()
            
                #import pdb; pdb.set_trace()
                connection.request('POST', '/1/classes/products', json.dumps(data), {
                       "X-Parse-Application-Id": self.application_id,
                       "X-Parse-REST-API-Key": self.rest_api_key,
                       "Content-Type": "application/json"
                     })
                results = json.loads(connection.getresponse().read())
                print results
            current_page = int(response.xpath("//div[@class='paging_b']/span/strong/text()").extract()[0])
            page_lists = response.xpath("//div[@class='paging_b']/span/a")
            for page in page_lists:
                if str(current_page+1) == page.xpath("text()").extract()[0]: 
                    next_links = self.base+page.xpath("@href").extract()[0]
                    yield scrapy.Request(url=next_links, callback=self.parse)
        
