import scrapy
import json,httplib

class QuotesSpider(scrapy.Spider):
    name = "lazada_category"
    start_urls = [
        'http://www.lazada.com.my/',
    ]
    application_id = "iVF27T7ctPbAy1FleUCi8BUhg0phe0DWndZUcPv4"
    rest_api_key = "hdh12Jvkpx2gDJwdlOKeWyIahtFcNst7gJs38Acf"
    
    def parse(self, response):
        
        categories = response.xpath('//div[@class="c-second-menu__item c-second-menu__item_style_heading1"]/a')
        for category in categories:
            #import pdb; pdb.set_trace()
            data = {
                'name': category.xpath('text()').extract()[0].strip(' \t\r\n'),
                'url': category.xpath('@href').extract()[0].split("/")[-2],
                'web': 'lazada',
            }
            connection = httplib.HTTPSConnection('api.parse.com', 443)
            connection.connect()
            connection.request('POST', '/1/classes/categories', json.dumps(data), {
                   "X-Parse-Application-Id": self.application_id,
                   "X-Parse-REST-API-Key": self.rest_api_key,
                   "Content-Type": "application/json"
                 })
            results = json.loads(connection.getresponse().read())
            print results
