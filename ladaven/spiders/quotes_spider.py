import scrapy
import json,httplib

class QuotesSpider(scrapy.Spider):
    name = "quotes"
    start_urls = [
        'http://quotes.toscrape.com/page/1/',
        'http://quotes.toscrape.com/page/2/',
    ]

    def parse(self, response):
        for quote in response.css('div.quote'):
            data = {
                'text': quote.css('span.text::text').extract_first(),
                'author': quote.css('span small::text').extract_first(),
                'tags': quote.css('div.tags a.tag::text').extract(),
            }
            connection = httplib.HTTPSConnection('api.parse.com', 443)
            connection.connect()
            connection.request('POST', '/1/classes/quotes', json.dumps(data), {
                   "X-Parse-Application-Id": "NxSRVhKxcgClL8cHne7figTHffslsa76K9ea2t5x",
                   "X-Parse-REST-API-Key": "w17uZrQAQYzMJJ7iTiuHw7g3h5IAqQjybNeRbs1m",
                   "Content-Type": "application/json"
                 })
            results = json.loads(connection.getresponse().read())
            print results
