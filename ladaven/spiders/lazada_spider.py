import scrapy
import json,httplib,urllib

class QuotesSpider(scrapy.Spider):
    
    name = "lazada"
    application_id = "iVF27T7ctPbAy1FleUCi8BUhg0phe0DWndZUcPv4"
    rest_api_key = "hdh12Jvkpx2gDJwdlOKeWyIahtFcNst7gJs38Acf"

    def start_requests(self):
        connection = httplib.HTTPSConnection('api.parse.com', 443)
        params = urllib.urlencode({"where":json.dumps({
           "web": "lazada"
         })})
        connection.connect()
        connection.request('GET', '/1/classes/categories?%s' % params,'',{
                        "X-Parse-Application-Id": self.application_id,
                        "X-Parse-REST-API-Key": self.rest_api_key,
                        "Content-Type": "application/json"
                        })
        data = json.loads(connection.getresponse().read())
        results = data['results']
        base = 'http://www.lazada.com.my/'
        for result in results:
            if result['web'] == 'lazada':
                yield scrapy.Request(url=base+result['url'], callback=self.parse)

    def parse(self, response):
        products = response.xpath('//div[@data-qa-locator="product-item"]')
        category = response.xpath('//h1[@class="c-catalog-title__title"]/text()').extract()[0].strip(' \t\n\r')
        if products:
            for product in products:
                description = product.xpath('a/div[@class="product-card__description"]')
                data = {
                    'name': description.xpath('div[@class="product-card__name-wrap"]/span/text()').extract()[0],
                    'price': description.xpath('div[@class="price-block--grid"]/div[@class="product-card__price"]/text()').extract()[0],
                    'category': category,
                    'url': product.xpath('a/@href').extract()[0].split("/")[3],
                    'web': 'lazada',
                }
                connection = httplib.HTTPSConnection('api.parse.com', 443)
                connection.connect()
            
                #import pdb; pdb.set_trace()
                connection.request('POST', '/1/classes/products', json.dumps(data), {
                       "X-Parse-Application-Id": self.application_id,
                        "X-Parse-REST-API-Key": self.rest_api_key,
                       "Content-Type": "application/json"
                     })
                results = json.loads(connection.getresponse().read())
                print results
            response_links = response.xpath('//span[@class="paging-wrapper"]/span/a[@class="next_link"]/@href').extract()
            if response_links:
                next_links = response_links[0]
                yield scrapy.Request(url=next_links, callback=self.parse)
        
